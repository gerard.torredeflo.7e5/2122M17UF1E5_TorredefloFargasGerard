﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    PlayerLandingCheck platform;
    bool platformReady = false;
    [SerializeField]
    float speed;
    [SerializeField]
    GameObject prefabLife;

    void Update()
    {
        if (platformReady == true)
        {
            playerLanded();
        }
    }

    void playerLanded()
    {
        if (platform.playerLanded == true)
        {
            HuntPlayer();
        }
    }

    void HuntPlayer()
    {
        float playerLocation = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position.x;
        if (playerLocation > transform.position.x)
        {
            var enemyLocation = transform.position;
            enemyLocation.x += speed * Time.deltaTime;
            transform.position = enemyLocation;
        }
        else if (playerLocation < transform.position.x)
        {
            var enemyLocation = transform.position;
            enemyLocation.x -= speed * Time.deltaTime;
            transform.position = enemyLocation;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Platform"))
        {
            platform = other.gameObject.GetComponent<PlayerLandingCheck>();
            platformReady = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (UnityEngine.Random.value > 0.6)
            {
                Vector3 lifeSpawnLocation = transform.position;
                lifeSpawnLocation.y += 0.5f;
                Instantiate(prefabLife, lifeSpawnLocation, Quaternion.identity);
            }
            GameManager.Instance.playerGotDamage = true;
            GameManager.Instance.defeatedEnemies += 1;
            platform.playerLanded = false;
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        platform.playerLanded = false;
    }
}

