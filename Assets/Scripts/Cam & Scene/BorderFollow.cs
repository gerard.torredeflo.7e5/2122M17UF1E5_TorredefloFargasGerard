﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderFollow : MonoBehaviour
{
    void Update()
    {
        var borderLocation = transform.position;
        float cameraLocation = GameObject.FindWithTag("MainCamera").GetComponent<Transform>().position.y;
        borderLocation.y = cameraLocation;
        transform.position = borderLocation;
    }
}
