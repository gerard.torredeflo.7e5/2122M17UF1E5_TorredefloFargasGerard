﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLandingCheck : MonoBehaviour
{
    public bool playerLanded = false;
    bool waitForPlayer = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (waitForPlayer == true)
        {
            playerLanded = true;
        }
        waitForPlayer = true;
    }
}
