﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    Vector3 startCamLocation;

    private void Start()
    {
        startCamLocation = transform.position;
    }
    void Update()
    {
        var cameraLocation = transform.position;
        float playerLocation = GameObject.FindWithTag("Player").GetComponent<Transform>().position.y;
        if (playerLocation >= 0.5)
        {
            if (playerLocation >= cameraLocation.y)
            {
                cameraLocation.y = playerLocation;
                transform.position = cameraLocation;
            }
        }
        if (GameManager.Instance.playerGotDamage == true)
        {
            transform.position = startCamLocation;
            GameManager.Instance.playerGotDamage = false;
        }
    }
}
