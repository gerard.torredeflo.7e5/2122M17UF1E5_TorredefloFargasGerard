﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceScore : MonoBehaviour
{
    void Start()
    {
        GetComponent<Text>().text = GameManager.Instance.distance.ToString();
    }
}
