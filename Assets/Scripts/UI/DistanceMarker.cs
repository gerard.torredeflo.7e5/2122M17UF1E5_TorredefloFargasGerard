﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceMarker : MonoBehaviour
{
    void Update()
    {
        GetComponent<Text>().text = GameManager.Instance.distance.ToString() + "m";
    }
}
