﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverController : MonoBehaviour
{
    [SerializeField]
    GameObject panelGO;

    void Update()
    {
        if (GameManager.Instance.gameOver == true)
        {
            panelGO.SetActive(true);
        }
    }
}
