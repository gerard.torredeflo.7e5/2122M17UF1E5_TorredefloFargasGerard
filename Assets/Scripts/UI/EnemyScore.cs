﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyScore : MonoBehaviour
{
    void Start()
    {
        GetComponent<Text>().text = GameManager.Instance.defeatedEnemies.ToString();
    }
}
