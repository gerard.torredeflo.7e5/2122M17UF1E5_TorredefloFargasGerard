﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _Instance;
    public static GameManager Instance
    {
        get
        {
            if (!_Instance)
            {
                _Instance = new GameObject().AddComponent<GameManager>();
                _Instance.name = _Instance.GetType().ToString();
                DontDestroyOnLoad(_Instance.gameObject);
            }
            return _Instance;
        }
    }

    public int defeatedEnemies = 0;
    public float distance = 0;
    public bool gameOver = false;
    public int lifes = 2;
    public bool playerGotDamage = false;

    public void GameOver()
    {
        gameOver = true;
    }

    public void ReloadGame()
    {
        distance = 0;
        defeatedEnemies = 0;
        lifes = 2;
        gameOver = false;
        SceneManager.LoadScene("Game");
    }
}
