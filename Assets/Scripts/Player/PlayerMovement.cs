﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    int speed;
    [SerializeField]
    int jumpForce;
    Rigidbody2D rigidbody;
    public bool isInTheAir;
    public Vector3 startingLocation;

    void Start()
    {
        transform.position = startingLocation;
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (GameManager.Instance.gameOver == false)
        {
            BasicMovement();
            Jump();
        }
    }

    void BasicMovement()
    {
        float inputX = Input.GetAxis("Horizontal");

        Vector3 movement = new Vector3(speed * inputX, 0);

        movement *= Time.deltaTime;

        transform.Translate(movement);
    }
    void Jump()
    {
        if (Input.GetKeyDown("space") && (isInTheAir == false))
        {
            rigidbody.AddForce(transform.up * jumpForce);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isInTheAir = false;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isInTheAir = true;
    }
}
