﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLifes : MonoBehaviour
{
    Vector3 startingLocation;

    private void Start()
    {
        startingLocation = GetComponent<PlayerMovement>().startingLocation;
    }

    void Update()
    {
        lifeChecker();
    }

    void lifeChecker()
    {
        if (GameManager.Instance.lifes == 0)
        {
            GameManager.Instance.GameOver();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Life") && GameManager.Instance.lifes < 3)
        {
            GameManager.Instance.lifes += 1;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            transform.position = startingLocation;
            GetComponent<PlayerMovement>().isInTheAir = false;
            GameManager.Instance.lifes -= 1;
        }
    }

    void OnBecameInvisible()
    {
        GameManager.Instance.lifes = 0;
    }
}
