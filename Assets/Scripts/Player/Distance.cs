﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distance : MonoBehaviour
{
    float startingPoint;
    float currentPoint;
    float highestPoint;
    int score;

    void Start()
    {
        startingPoint = transform.position.y;
        highestPoint = startingPoint;
    }

    void Update()
    {
        currentPoint = transform.position.y;
        if (currentPoint >= highestPoint + 1)
        {
            highestPoint = currentPoint;
            score += 1;
            GameManager.Instance.distance = score;
        }
    }
}
